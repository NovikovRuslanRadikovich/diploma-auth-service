package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kpfu.itis.config.UrlsConfig;
import ru.kpfu.itis.dto.SignInDto;
import ru.kpfu.itis.service.UserService;

import javax.xml.bind.DatatypeConverter;

@RestController
public class SignInController {

    UrlsConfig urlsConfig;
    UserService userService;
    ShaPasswordEncoder passwordEncoder;

    @Autowired
    SignInController(UrlsConfig urlsConfig, UserService userService, ShaPasswordEncoder passwordEncoder) {
        this.urlsConfig = urlsConfig;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @RequestMapping(value = "/sign_in", method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody SignInDto signInDto) {
        return userService.authenticate(signInDto);
    }
}
