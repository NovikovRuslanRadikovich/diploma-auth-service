package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.service.MailSender;
import ru.kpfu.itis.service.UserService;

@RestController
public class PasswordUpdateController {

    UserService userService;
    MailSender mailSender;

    @Autowired
    PasswordUpdateController(UserService userService, MailSender mailSender) {
        this.userService = userService;
        this.mailSender = mailSender;
    }

    @RequestMapping(value = "/updatePassword", method = RequestMethod.GET)
    public ResponseEntity changePassword(@RequestParam("mailbox") String mailbox) {

        boolean wasPasswordChanged = userService.changePassword(mailbox);
        if(!wasPasswordChanged) {
            return ResponseEntity.badRequest().body("Failed to change password");
        }

        return ResponseEntity.ok().body("Password was changed successfully");
    }



}
