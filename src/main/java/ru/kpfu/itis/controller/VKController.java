package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.dto.VKSignUpDto;
import ru.kpfu.itis.dto.VKUserDto;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.service.VKUserService;
import ru.kpfu.itis.util.VKUserValidityInfo;

@RestController
public class VKController {

    VKUserService vkUserService;

    @Autowired
    VKController(VKUserService vkUserService) {
        this.vkUserService = vkUserService;
    }

    @RequestMapping(value = "/vk_sign_up", method = RequestMethod.POST)
    public ResponseEntity registerViaVkToken(@RequestBody VKSignUpDto vkSignUpDto) {
        //I CHECK IF RECEIVED ACCESS_TOKEN CORRESPONDS TO TOKEN WHICH IS STORED IN DB AFTER FIRST AUTHENTICATION
        VKUserDto vkUserDto = vkSignUpDto.getVkUserDto();
        VKUserValidityInfo vkUserValidityInfo = vkUserService.getVkUserValidityInfo(vkUserDto);
        if(vkUserValidityInfo.equals(VKUserValidityInfo.VKUsersTokenISExpired)) {
            return ResponseEntity.badRequest().body("Your vk service access token is expired");
        } else if (vkUserValidityInfo.equals(VKUserValidityInfo.NoSuchVKUserISStored)){
            return ResponseEntity.badRequest().body("You are sending not valid vk token");
        }
        //IF SENT ACCESS_TOKEN IS VALID, THEN I TRY TO REGISTER NEW USER
        //NOW VK_ACCOUNT IS TRANSFERRED AND I HAVE TO SET NEW USER TO THIS VK_ACCOUNT
        vkUserService.createNewUserSettedToVk(vkSignUpDto);

        return ResponseEntity.ok("saved vk user");
    }

    @RequestMapping(value = "/vk", method = RequestMethod.GET, params = "code")
    public ResponseEntity receiveCode(@RequestParam("code") String code) {
        return vkUserService.authenticateVkUserAfterCodeReceiving(code);
    }

}