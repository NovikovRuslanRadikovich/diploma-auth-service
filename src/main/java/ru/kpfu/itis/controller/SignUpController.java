package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.UserResponseDto;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.util.Error;

@RestController
public class SignUpController {

    @Autowired
    UserService userService;

    @Autowired
    TokenStore tokenStore;

    @RequestMapping(value = "/sign_up", method = RequestMethod.POST)
    public ResponseEntity signUp(@RequestBody SignUpDto signUpDto) {
        if (userService.isAlsoRegisteredWithMailbox(signUpDto.getMailbox())) {
            return ResponseEntity
                    .badRequest()
                    .body(Error.builder()
                            .error_message("user with this mailbox is also registered in system")
                            .build()
                    );

        }
        UserResponseDto userResponseDto = userService.createUser(signUpDto);
        return ResponseEntity.ok(userResponseDto);
    }


    @RequestMapping(value = "/activate/{code}", method = RequestMethod.GET)
    public ResponseEntity activate(@PathVariable String code) {
        Object isActivated = userService.activateUser(code);
        if (isActivated instanceof Boolean) {
            if(!((Boolean) isActivated)) {
                return ResponseEntity
                        .badRequest()
                        .body(Error.builder()
                                .error_message("Activation code is not found!")
                                .build()
                        );
            }
        }
        return ResponseEntity.ok(isActivated);

    }

}
