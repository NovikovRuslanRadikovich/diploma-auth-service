package ru.kpfu.itis.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:vk_application.properties")
@Getter
public class VKApplicationConfig {

    @Value("${client_id}")
    String clientId;
    @Value("${client_secret}")
    String clientSecret;
}