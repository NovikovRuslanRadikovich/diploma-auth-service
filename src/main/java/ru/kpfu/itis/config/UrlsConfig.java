package ru.kpfu.itis.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:urls.properties")
@Getter
public class UrlsConfig {

    @Value("${oauth.token.default.url}")
    String oauthDefaultTokenUrl;
    @Value("${oauth.code.redirect.url}")
    String oauthCodeRedirectUrl;
    @Value("${oauth.vk.access_token.url}")
    String vkAccessTokenUrl;
    @Value("${oauth.application.domain}")
    String oauthApplicationDomain;
}
