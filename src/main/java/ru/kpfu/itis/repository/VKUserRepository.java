package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.entity.VKUser;

@Repository
public interface VKUserRepository extends JpaRepository<VKUser,Long> {

    VKUser findVKUserByVk(Integer vk);

    VKUser getVKUserByVkAndAccessToken(Integer vk,String accessToken);

    VKUser save(VKUser user);

}
