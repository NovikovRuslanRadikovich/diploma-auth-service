package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Boolean existsByMailbox(String mail);

    User findByMailbox(String email);

    User findByActivationCode(String code);

    User save(User user);

    User findById(Integer id);
}
