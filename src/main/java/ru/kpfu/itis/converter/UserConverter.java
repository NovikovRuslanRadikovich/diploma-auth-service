package ru.kpfu.itis.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.UserResponseDto;
import ru.kpfu.itis.entity.User;

import static java.util.Objects.isNull;

@Component
public class UserConverter {

    @Autowired
    ShaPasswordEncoder passwordEncoder;

    public UserResponseDto doForward(User user) {
        if (isNull(user)) {
            return null;
        }
        return UserResponseDto.builder()
                .id(user.getId())
                .mailbox(user.getMailbox())
                .username(user.getUsername())
                .build();
    }

    public User doBackward(SignUpDto userDto) {
        if (isNull(userDto)) {
            return null;
        }
        return User.builder()
                .mailbox(userDto.getMailbox())
                .password(passwordEncoder.encodePassword(userDto.getPassword(),userDto.getMailbox()))
                .build();
    }

}
