package ru.kpfu.itis.converter;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.VKSignUpDto;
import ru.kpfu.itis.dto.VKUserDto;
import ru.kpfu.itis.entity.VKUser;

import java.io.IOException;

import static java.util.Objects.isNull;

@Component
public class VKUserConverter {

    public VKUserDto doForward(VKUser vkUser) {
        if (isNull(vkUser)) {
            return null;
        }
        return VKUserDto.builder()
                .vk(vkUser.getVk())
                .access_token(vkUser.getAccessToken())
                .expires_in(vkUser.getExpirationTime())
                .user_id(vkUser.getVkUserId())
                .build();
    }

    public VKUser doBackward(VKSignUpDto vkSignUpDto) {
        if (isNull(vkSignUpDto)) {
            return null;
        }
        return VKUser.builder()
                .vk(vkSignUpDto.getVkUserDto().getVk())
                .accessToken(vkSignUpDto.getVkUserDto().getAccess_token())
                .expirationTime(vkSignUpDto.getVkUserDto().getExpires_in())
                .vkUserId(vkSignUpDto.getVkUserDto().getUser_id())
                .build();
    }

    public VKUserDto convertUserToVKUserDto(ResponseEntity<String> vkJson) {
        ObjectMapper mapper = new ObjectMapper();
        VKUserDto vkUserDto = new VKUserDto();
        try {
            vkUserDto = mapper.readValue(vkJson.getBody(), VKUserDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vkUserDto;
    }

}
