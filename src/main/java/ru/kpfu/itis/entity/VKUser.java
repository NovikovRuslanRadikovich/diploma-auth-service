package ru.kpfu.itis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table
public class VKUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer vk;
    private String accessToken;
    private Integer expirationTime;
    private Integer vkUserId;
    private Timestamp creationTime;
    @OneToOne(fetch = FetchType.EAGER)
    private User user;
}
