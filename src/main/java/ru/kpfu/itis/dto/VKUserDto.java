package ru.kpfu.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VKUserDto {

    private Integer vk;
    private String access_token;
    private Integer expires_in;
    private Integer user_id;
    private UserResponseDto userResponseDto;
}
