package ru.kpfu.itis.service;

import org.springframework.http.ResponseEntity;
import ru.kpfu.itis.dto.SignInDto;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.UserResponseDto;
import ru.kpfu.itis.entity.User;

public interface UserService {

    Boolean isAlsoRegisteredWithMailbox(String mail);

    UserResponseDto createUser(SignUpDto signUpDto);

    boolean changePassword(String mailbox);

    Object activateUser(String code);

    UserResponseDto findByMailBox(String email);

    void changeUserPassword(User user, String newPassword);

    public String getEncodedPasswordByMailbox(String email);

    public User findById(Integer id);

    public ResponseEntity authenticate(SignInDto signInDto);
}
