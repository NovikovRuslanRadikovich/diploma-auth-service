package ru.kpfu.itis.service;

import org.springframework.http.ResponseEntity;
import ru.kpfu.itis.dto.VKSignUpDto;
import ru.kpfu.itis.dto.VKUserDto;
import ru.kpfu.itis.util.VKUserValidityInfo;

public interface VKUserService {

    VKUserDto getById(Integer id);

    VKUserValidityInfo getVkUserValidityInfo(VKUserDto vkUserDto);

    VKUserDto transferNewVKAccount(VKUserDto vkUserDto);

    boolean createNewUserSettedToVk(VKSignUpDto vkSignUpDto);

    ResponseEntity authenticateVkUserAfterCodeReceiving(String code);
}
