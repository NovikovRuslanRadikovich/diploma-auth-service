package ru.kpfu.itis.service.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kpfu.itis.config.UrlsConfig;
import ru.kpfu.itis.converter.UserConverter;
import ru.kpfu.itis.dto.SignInDto;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.UserResponseDto;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.repository.UserRepository;
import ru.kpfu.itis.service.MailSender;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.util.Role;

import javax.xml.bind.DatatypeConverter;
import java.util.UUID;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

    private UserConverter converter;
    private UserRepository userDao;
    private MailSender mailSender;
    private ShaPasswordEncoder passwordEncoder;
    private UrlsConfig urlsConfig;

    @Autowired
    public UserServiceImpl(UserConverter converter, UserRepository userDao, MailSender mailSender, ShaPasswordEncoder passwordEncoder, UrlsConfig urlsConfig) {
        this.converter = converter;
        this.userDao = userDao;
        this.mailSender = mailSender;
        this.passwordEncoder = passwordEncoder;
        this.urlsConfig = urlsConfig;
    }

    @Override
    public User loadUserByUsername(String mail) throws UsernameNotFoundException {
        User user = userDao.findByMailbox(mail);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return user;
    }

    @Override
    public Boolean isAlsoRegisteredWithMailbox(String mail) {
        return userDao.existsByMailbox(mail);
    }

    @Override
    public UserResponseDto createUser(SignUpDto signUpDto) {
        User user = converter.doBackward(signUpDto);
        user.setIsenabled(false);
        user.setRole(Role.USER);
        user.setActivationCode(UUID.randomUUID().toString());
        user = userDao.save(user);

        if (!StringUtils.isEmpty(user.getMailbox())) {
            String message = String.format(
                    "Hello, %s! \n" +
//TODO change link according to server domain
                            "Welcome to Students Employment Application. Please, visit next link: " + urlsConfig.getOauthApplicationDomain() + "/activate/%s",
                    user.getUsername(),
                    user.getActivationCode()
            );
            mailSender.send(user.getMailbox(), "Activation code", message);
        }
        return converter.doForward(user);
    }


    @Override
    public boolean changePassword(String mailbox) {
        User userFromDb = userDao.findByMailbox(mailbox);
        if (userFromDb == null) {
            return false;
        } else {
            char[] possibleCharacters = ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]},<.>/?").toCharArray();
            String newPassword = RandomStringUtils.random(8, possibleCharacters);
            userFromDb.setPassword(passwordEncoder.encodePassword(newPassword,mailbox));
            userDao.save(userFromDb);
            if (!StringUtils.isEmpty(userFromDb.getMailbox())) {
                String message = String.format(
                        "Hello, %s! \n" +
                                "Your new password is: %s",
                        userFromDb.getUsername(),
                        newPassword
                );
                mailSender.send(userFromDb.getMailbox(), "New password", message);
            }
        }
        return true;
    }

    @Override
    public Object activateUser(String code) {
        User user = userDao.findByActivationCode(code);
        if (user == null) {
            return false;
        }
        user.setIsenabled(true);
        user.setActivationCode(null);
        userDao.save(user);
        RestTemplate restTemplate = new RestTemplate();
        String authorizationHeader = "Basic " + DatatypeConverter.printBase64Binary(("employment-client" + ":" + "employment-secret").getBytes());
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Authorization", authorizationHeader);
        requestHeaders.add("Accept", MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlsConfig.getOauthDefaultTokenUrl())
                .queryParam("username", user.getMailbox())
                .queryParam("grant_type", "password")
                .queryParam("password", user.getPassword());

        return restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                requestEntity,
                Object.class);
    }

    @Override
    public UserResponseDto findByMailBox(String email) {
        return converter.doForward(userDao.findByMailbox(email));
    }

    @Override
    public void changeUserPassword(User user, String newPassword) {
        user.setPassword(passwordEncoder.encodePassword(newPassword,user.getMailbox()));
        userDao.save(user);
    }

    public String getEncodedPasswordByMailbox(String email) {
        return userDao.findByMailbox(email).getPassword();
    }

    @Override
    public User findById(Integer id) {
        return userDao.findById(id);
    }

    @Override
    public ResponseEntity authenticate(SignInDto signInDto) {
        RestTemplate restTemplate = new RestTemplate();
        String authorizationHeader = "Basic " + DatatypeConverter.printBase64Binary(("employment-client" + ":" + "employment-secret").getBytes());
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Authorization", authorizationHeader);
        requestHeaders.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlsConfig.getOauthDefaultTokenUrl())
                .queryParam("username", signInDto.getMailbox())
                .queryParam("grant_type", "password")
                .queryParam("password", passwordEncoder.encodePassword(signInDto.getPassword(), signInDto.getMailbox())
                );

        ResponseEntity response;
        try {
            response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.POST,
                    requestEntity,
                    Object.class);
        } catch(HttpClientErrorException exception) {
            return ResponseEntity.status(400).contentType(MediaType.TEXT_PLAIN).body("Bad Credentials");
        }
        return response;
    }

}
