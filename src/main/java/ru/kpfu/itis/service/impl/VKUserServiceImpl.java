package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kpfu.itis.config.UrlsConfig;
import ru.kpfu.itis.config.VKApplicationConfig;
import ru.kpfu.itis.converter.UserConverter;
import ru.kpfu.itis.converter.VKUserConverter;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.UserResponseDto;
import ru.kpfu.itis.dto.VKSignUpDto;
import ru.kpfu.itis.dto.VKUserDto;
import ru.kpfu.itis.entity.VKUser;
import ru.kpfu.itis.repository.VKUserRepository;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.service.VKUserService;
import ru.kpfu.itis.util.VKUserValidityInfo;

import javax.xml.bind.DatatypeConverter;
import java.sql.Timestamp;

@Service
public class VKUserServiceImpl implements VKUserService {

    UrlsConfig urlsConfig;
    VKApplicationConfig vkApplicationConfig;
    VKUserRepository vkUserRepository;
    UserConverter userConverter;
    UserService userService;
    VKUserConverter vkUserConverter;

    @Autowired
    VKUserServiceImpl(UrlsConfig urlsConfig, VKApplicationConfig vkApplicationConfig, VKUserRepository vkUserRepository, UserConverter userConverter, UserService userService, VKUserConverter vkUserConverter) {
        this.urlsConfig = urlsConfig;
        this.vkApplicationConfig = vkApplicationConfig;
        this.vkUserRepository = vkUserRepository;
        this.userConverter = userConverter;
        this.userService = userService;
        this.vkUserConverter = vkUserConverter;
    }

    @Override
    public VKUserDto getById(Integer vk) {
        return vkUserConverter.doForward(vkUserRepository.findVKUserByVk(vk));
    }

    @Override
    public boolean createNewUserSettedToVk(VKSignUpDto vkSignUpDto) {

        VKUserDto vkUserDto = vkSignUpDto.getVkUserDto();
        SignUpDto signUpDto = vkSignUpDto.getSignUpDto();
        //CHECK THAT USER WITH SUCH EMAIL DOESN'T EXIST IN DATABASE
        UserResponseDto userByMailbox = userService.findByMailBox(signUpDto.getMailbox());
        if (userByMailbox != null) {
            return false;
        }

        UserResponseDto userResponseDto = userService.createUser(signUpDto);

        VKUser vkUser = VKUser
                .builder()
                .vk(vkUserDto.getVk())
                .accessToken(vkUserDto.getAccess_token())
                .expirationTime(vkUserDto.getExpires_in())
                .user(userService.findById(userResponseDto.getId()))
                .build();
        vkUserRepository.save(vkUser);
        return true;
    }

    @Override
    public VKUserValidityInfo getVkUserValidityInfo(VKUserDto vkUserDto) {
        VKUser vkUser = VKUser.builder().vk(vkUserDto.getVk()).accessToken(vkUserDto.getAccess_token()).expirationTime(vkUserDto.getExpires_in()).build();
        VKUser vkUserFromDB = vkUserRepository.getVKUserByVkAndAccessToken(vkUser.getVk(), vkUser.getAccessToken());
        if (vkUserFromDB == null) {
            return VKUserValidityInfo.NoSuchVKUserISStored;
        }
        long currentTimeInSeconds = System.currentTimeMillis() / 1000;
        long timeOfVKUserCreationInSeconds = vkUserFromDB.getCreationTime().getTime() / 1000;
        if (currentTimeInSeconds <= timeOfVKUserCreationInSeconds + vkUser.getExpirationTime()) {
            return VKUserValidityInfo.VKUserISValid;
        }
        return VKUserValidityInfo.VKUsersTokenISExpired;
    }

    @Override
    public VKUserDto transferNewVKAccount(VKUserDto vkUserDto) {
        VKUser vkUser = VKUser
                .builder()
                .vk(vkUserDto.getVk())
                .accessToken(vkUserDto.getAccess_token())
                .expirationTime(vkUserDto.getExpires_in())
                .creationTime(new Timestamp(System.currentTimeMillis()))
                .vkUserId(vkUserDto.getUser_id())
                .build();

        return vkUserConverter.doForward(vkUserRepository.save(vkUser));
    }


    @Override
    public ResponseEntity authenticateVkUserAfterCodeReceiving(String code) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders vkRequestHeaders = new HttpHeaders();
        vkRequestHeaders.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> vkRequestEntity = new HttpEntity<>(vkRequestHeaders);
        UriComponentsBuilder vkBuilder = UriComponentsBuilder.fromHttpUrl(urlsConfig.getVkAccessTokenUrl())
                .queryParam("code", code)
                .queryParam("redirect_uri",
                        urlsConfig.getOauthCodeRedirectUrl()
                )
                .queryParam("client_id",
                        vkApplicationConfig.getClientId()
                )
                .queryParam("client_secret",
                        vkApplicationConfig.getClientSecret()
                );
        ResponseEntity<String> result = restTemplate.exchange(
                vkBuilder.toUriString(),
                HttpMethod.GET,
                vkRequestEntity,
                String.class);
        VKUserDto vkUserDto = vkUserConverter.convertUserToVKUserDto(result);
        //If there is no user with current id from vk
        if (getById(vkUserDto.getVk()) == null) {
            VKUserDto transferedVkUserAccount = transferNewVKAccount(vkUserDto);
            return ResponseEntity.ok().body(transferedVkUserAccount);
        }
        //If user with current id from vk is registered in application
        else {
            UserResponseDto userResponseDto = getById(vkUserDto.getVk()).getUserResponseDto();
            //PASSWORD ENCODING
            String usersEncodedPassword = userService.getEncodedPasswordByMailbox(userResponseDto.getMailbox());
            String authorizationHeader = "Basic " + DatatypeConverter.printBase64Binary(("employment-client" + ":" + "employment-secret").getBytes());
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Authorization", authorizationHeader);
            requestHeaders.add("Accept", MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
                    urlsConfig.getOauthDefaultTokenUrl()
            )
                    .queryParam("username", userResponseDto.getMailbox())
                    .queryParam("grant_type", "password")
                    .queryParam("password", usersEncodedPassword);

            return restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.POST,
                    requestEntity,
                    Object.class);
        }
    }

}
