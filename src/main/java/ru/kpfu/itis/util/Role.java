package ru.kpfu.itis.util;

public enum Role {
    USER, ADMIN, MODERATOR
}
