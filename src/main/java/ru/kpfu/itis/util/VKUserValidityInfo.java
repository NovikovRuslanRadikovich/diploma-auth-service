package ru.kpfu.itis.util;

public enum VKUserValidityInfo {

    VKUserISValid,
    VKUsersTokenISExpired,
    NoSuchVKUserISStored
}
